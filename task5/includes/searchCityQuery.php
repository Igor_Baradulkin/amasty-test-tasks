<?php
    require_once 'dbconn.inc.php';

    if(isset($_POST['search-city'])){
        $query = "SELECT cities.name, COUNT(cities.name) as count
                  FROM cities 
                  JOIN persons ON persons.city_id = cities.id 
                  JOIN transactions ON transactions.from_person_id = persons.id 
                  GROUP BY cities.name 
                  ORDER BY COUNT(cities.name) DESC LIMIT 1";

        $result = mysqli_query($dbConn, $query);

        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
              echo "City name: " . $row["name"] . "<br>Transactions count: " . $row['count'];
            }
          } else {
            echo "0 results";
          }
          
          mysqli_close($dbConn);
    }  
?>


