<?php
    require_once 'dbconn.inc.php';

    if(isset($_POST['search-transaction'])){
        $query = "SELECT transactions.transaction_id ,transactions.from_person_id, transactions.to_person_id, transactions.amount
                  FROM transactions 
                  JOIN persons as p1 ON p1.id = transactions.from_person_id 
                  JOIN persons as p2 ON p2.id = transactions.to_person_id AND p1.city_id = p2.city_id";

        $result = mysqli_query($dbConn, $query);

        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
              echo 'Transaction id: '.$row['transaction_id']."<br>from_person_id: ".$row['from_person_id']."<br>to_person_id: ".$row['to_person_id']."<br>Amount: ".$row['amount']."<br>";
              echo "-------------------------<br>";
            }
          } else {
            echo "0 results";
          }
          
          mysqli_close($dbConn);
    }  

?>

