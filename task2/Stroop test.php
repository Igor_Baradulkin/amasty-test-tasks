<title>Stroop test</title>
<form action="Stroop test.php" method="post">
    <button type="submit" name="button">Generate Stroop Test</button>
</form>
<link rel="stylesheet" href="styles.css">
<div>
    <?php
    class StroopColor{
        static $colorNameArray = [];
        static $colorsArray = [];

        public $colorName;
        public $color;

        function __construct(){
            global $colors;
            $index1 = rand(0, 9);
            $this->colorName = $colors[$index1];
        
            if(in_array($this->colorName, self::$colorNameArray)){
                while(in_array($this->colorName, self::$colorNameArray)){
                    $index1 = rand(0, 9);
                    $this->colorName = $colors[$index1];                
                }
                self::$colorNameArray[] = $this->colorName;
            }
            else{
                self::$colorNameArray[] = $this->colorName;
            }

            $index2 = rand(0, 9);
            $this->color = $colors[$index2];

            if($index1 == $index2 || in_array($this->color, self::$colorsArray)){
                while($index1 == $index2 || in_array($this->color, self::$colorsArray)){
                    $index2 = rand(0, 9);
                    $this->color = $colors[$index2];
                    if($index1 != $index2 && !in_array($this->color, self::$colorsArray)){
                        break;
                    }
                }
            }
            self::$colorsArray[] = $this->color;
        }
    }

    $colors = ["red", "blue", "green", "yellow", "lime", "magenta", "black", "gold", "gray", "tomato"];

    if(isset($_POST['button'])){
        $color1 = new StroopColor();
        $color2 = new StroopColor();
        $color3 = new StroopColor();
        $color4 = new StroopColor();
        $color5 = new StroopColor();   

        echo "<p style=color:".$color1->color.">".$color1->colorName."</p>";
        echo "<p style=color:".$color2->color.">".$color2->colorName."</p>";
        echo "<p style=color:".$color3->color.">".$color3->colorName."</p>";
        echo "<p style=color:".$color4->color.">".$color4->colorName."</p>";
        echo "<p style=color:".$color5->color.">".$color5->colorName."</p>";
    }
    ?>
</div>
