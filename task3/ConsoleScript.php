<?php
    function bubleSortArr($array){
        for($i = count($array) - 1; $i >= 1; $i--){
            for($j = 0; $j < $i; $j++){
                if($array[$j] > $array[$j + 1]){
                    $tmp = $array[$j];
                    $array[$j] = $array[$j + 1];
                    $array[$j + 1] = $tmp;
                }
            }
        }

        return $array;
    }    

    $charsArr = explode(" " , $argv[1]);

    $numArr = [];

    foreach($charsArr as $element){
       if(preg_match('/^(0|-??[1-9][0-9]*)$/', $element)){
            if(!in_array($element, $numArr)){
                $numArr[] = $element;
            }            
       }
    }

    print_r(bubleSortArr($numArr));
?>