<title>Football site parser</title>
<form action="Football site parser.php" method="post">
    <input type="text" name="team-name" placeholder="Enter the name of the team">
    <button type="submit" name="find-team">Search team</button>
</form>

<?php 
    include_once 'simple_html_dom.php';

    $archiveHTML = file_get_html("https://terrikon.com/football/italy/championship/archive");

    if(isset($_POST['find-team'])){
        $commandForSearch = $_POST['team-name'];
        $resultsAmount = 0;

        foreach($archiveHTML->find('div[class=tab]') as $seasons){
            foreach($seasons->find('a[href^=/football/italy/championship]') as $teamArchiveLink){
                $seasonPeriod = substr($teamArchiveLink->innertext, 0, 7);
                $seasonStatisticPage = file_get_html("https://terrikon.com".$teamArchiveLink->href);
                $championshipTable = $seasonStatisticPage->find('table[class=colored big]')[0];
                
                foreach(array_slice($championshipTable->find('tr'), 1) as $teamStat){
                    $place = $teamStat->find('td')[0]->innertext;
                    $team = $teamStat->find('td')[1]->plaintext;
                    if($team == $commandForSearch){
                        echo 'Season: '.$seasonPeriod.'. Place: '.$place.'<br>';
                        $resultsAmount++;
                    }
                }
            }
        }

        if($resultsAmount == 0){
            echo "По вашему запрому не найдено результатов для данной команды";
        }
}   
?>
